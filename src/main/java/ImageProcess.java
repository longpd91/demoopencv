import org.ghost4j.document.PDFDocument;
import org.ghost4j.renderer.SimpleRenderer;
import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class ImageProcess {

  private static final String ROOT_PATH = "D:/LONGPD/Git_Project/DemoOpenCV/cards/";
  private static final String PDF_FILE_PATH = ROOT_PATH + "pdf/";
  private static final String IMG_FILE_PATH = ROOT_PATH + "image/";
  private static final String ENHANCED_IMG_FILE_PATH = ROOT_PATH + "enhanced_image/";

  static {
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
  }

  public static void main(String[] args) {
    File imgFolder = new File(IMG_FILE_PATH);
    if (!imgFolder.exists()) {
      imgFolder.mkdirs();
    }
    File[] imgFiles = imgFolder.listFiles();

    if (imgFiles == null || imgFiles.length == 0) {
      convertManyPDF2Image();
    }
    imgFiles = imgFolder.listFiles();
    System.out.println("imgFiles=" + imgFiles.length);
    for (File imgFile : imgFiles) {
      if (!imageProcess3(imgFile.getName())) {
        return;
      }
    }
  }

  private static boolean convertManyPDF2Image() {
    File pdfFolder = new File(PDF_FILE_PATH);
    if (!pdfFolder.exists()) {
      pdfFolder.mkdirs();
    }
    String[] pdfFilesName = pdfFolder.list();
    if (pdfFilesName == null || pdfFilesName.length == 0) return false;
    for (String pdfFileName : pdfFilesName) {
      System.out.println(pdfFileName);
      if (!convertPDF2Image(pdfFileName, IMG_FILE_PATH)) return false;
    }
    return true;
  }

  public static boolean convertPDF2Image(String pdfFileName, String imageRootPath) {
    try {
      // load PDF document
      PDFDocument document = new PDFDocument();
      document.load(new File(PDF_FILE_PATH + pdfFileName));
      // create renderer
      SimpleRenderer renderer = new SimpleRenderer();
      // set resolution (in DPI)
      renderer.setResolution(200);
      renderer.setMaxProcessCount(2);
      // copy file gsdll64.dll vao C:\Windows\System32
      // render
      List<Image> images = renderer.render(document);
      String imageFileName = pdfFileName.replace(".pdf", "");
      if (images == null) return false;
      // write images to files to disk as PNG
      for (int i = 0; i < images.size(); i++) {
        ImageIO.write((RenderedImage) images.get(i), "png",
            new File(imageRootPath + File.separator + imageFileName + "_" + (i + 1) + ".png"));
      }
    } catch (Exception e) {
      System.out.println("ERROR: " + e.getMessage());
      return false;
    }
    return true;
  }

  public static boolean imageProcess(String imgFileName) {
    System.out.println(imgFileName);
    try {
      File enhandcedFolder = new File(ENHANCED_IMG_FILE_PATH);
      if (!enhandcedFolder.exists()) {
        enhandcedFolder.mkdirs();
      }
      String imgInputPath = IMG_FILE_PATH + imgFileName;
      String imgOutputPath = ENHANCED_IMG_FILE_PATH + imgFileName.replace(".png", "");
      System.out.println("Line 1");
      Mat source = Imgcodecs.imread(imgInputPath, Imgcodecs.CV_LOAD_IMAGE_GRAYSCALE);
      if (source.empty()) {
        System.out.println("Error: no image found!");
        return false;
      }

      System.out.println("Line 2");
      Mat imgGray = new Mat(source.rows(), source.cols(), source.type());
      source.copyTo(imgGray);
      Imgcodecs.imwrite(imgOutputPath + "_gray.png", imgGray);

      System.out.println("Line 3");
      Mat imgThreshold = new Mat(source.rows(), source.cols(), source.type());
//    Imgproc.threshold(source, imgThreshold, 125, 255, Imgproc.THRESH_BINARY);
      Imgproc.adaptiveThreshold(source, imgThreshold, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C,
          Imgproc.THRESH_BINARY_INV, 15, 60);

      System.out.println("Line 4");
      Mat imgGaussian = new Mat(source.rows(), source.cols(), source.type());
      Imgproc.GaussianBlur(imgThreshold, imgGaussian, new Size(5, 5), 0);
      Imgcodecs.imwrite(imgOutputPath + "_gaussian.png", imgGaussian);

      Mat imgMorphology = new Mat(source.rows(), source.cols(), source.type());
      Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(2, 2));
      Imgproc.morphologyEx(imgThreshold, imgMorphology, Imgproc.MORPH_OPEN, kernel);
      Imgcodecs.imwrite(imgOutputPath + "_morphology.png", imgMorphology);

      Rect rect;
      for (int i = 0; i < imgThreshold.width() - 16; i += 8) {
        for (int j = 0; j < imgThreshold.height() - 8; j += 8) {
          rect = new Rect(i, j, 16, 8);
          //Get ROI
          Mat imageROI = imgThreshold.submat(rect);


//          Imgproc.rectangle(imgThreshold, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
//              new Scalar(255, 0, 0));
        }
      }
/*
      List<MatOfPoint> contours = new ArrayList<>();
//      Mat dest = Mat.zeros(imgThreshold.size(), CvType.CV_8UC3);
      Scalar white = new Scalar(255, 0, 0);
      // Find contours
      Imgproc.findContours(imgThreshold, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
      System.out.println("contours=" + contours.size());
      // Draw contours in dest Mat
      Imgproc.drawContours(imgThreshold, contours, 1, white);
      for (MatOfPoint contour : contours) {
//        Imgproc.fillPoly(imgThreshold, Arrays.asList(contour), white);


        MatOfPoint2f areaPoints = new MatOfPoint2f(contour.toArray());
        RotatedRect boundingRect = Imgproc.minAreaRect(areaPoints);

        double rectangleArea = boundingRect.size.area();

        // test min ROI area in pixels
        if (rectangleArea > 500) {
          Point rotated_rect_points[] = new Point[4];
          boundingRect.points(rotated_rect_points);

          Rect rect1 = Imgproc.boundingRect(new MatOfPoint(rotated_rect_points));

          // test horizontal ROI orientation
          if (rect1.width > rect1.height) {
            Imgproc.rectangle(imgThreshold, rect1.tl(), rect1.br(), white, 1);
          }
        }
      }
*/
      Imgcodecs.imwrite(imgOutputPath + "_threshold.png", imgThreshold);

//      Mat imgThresholdGaussian = new Mat(source.rows(), source.cols(), source.type());
//      Imgproc.GaussianBlur(imgThreshold, imgThresholdGaussian, new Size(5, 5), 0);
//      Imgcodecs.imwrite(imgOutputPath + "_threshold_gaussian.png", imgThresholdGaussian);

      System.out.println("Line 5");
      Mat imgThreshold2 = new Mat(source.rows(), source.cols(), source.type());
      Imgproc.threshold(source, imgThreshold2, 127, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C);

      Imgcodecs.imwrite(imgOutputPath + "_threshold_2.png", imgThreshold2);

      System.out.println("Line 6");
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }

    return true;
  }

  public static boolean imageProcess2(String imgFileName) {
    try {
      File enhandcedFolder = new File(ENHANCED_IMG_FILE_PATH);
      if (!enhandcedFolder.exists()) {
        enhandcedFolder.mkdirs();
      }
      String imgInputPath = IMG_FILE_PATH + imgFileName;
      String imgOutputPath = ENHANCED_IMG_FILE_PATH + imgFileName.replace(".png", "");
      System.out.println("Line 1");
      Mat source = Imgcodecs.imread(imgInputPath, Imgcodecs.CV_LOAD_IMAGE_COLOR);
      if (source.empty()) {
        System.out.println("Error: no image found!");
        return false;
      }
      Mat src = new Mat();
      Imgproc.cvtColor(source, src, Imgproc.COLOR_BGR2RGB);//convert source image to src color
      Mat blurred = src.clone();
      Imgproc.medianBlur(src, blurred, 9);

      Mat gray0 = new Mat(blurred.size(), CvType.CV_8U), gray = new Mat();
      List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

      List<Mat> blurredChannel = new ArrayList<Mat>();
      blurredChannel.add(blurred);
      List<Mat> gray0Channel = new ArrayList<Mat>();
      gray0Channel.add(gray0);

      MatOfPoint2f approxCurve;

      double maxArea = 0;
      int maxId = -1;


      for (int c = 0; c < 3; c++) {
        int ch[] = {c, 0};
        Core.mixChannels(blurredChannel, gray0Channel, new MatOfInt(ch));

        int thresholdLevel = 1;
        for (int t = 0; t < thresholdLevel; t++) {
          if (t == 0) {
            Imgproc.Canny(gray0, gray, 10, 20, 3, true); // true ?
            Imgproc.dilate(gray, gray, new Mat(), new Point(-1, -1), 1); // 1
            // ?
          } else {
            Imgproc.adaptiveThreshold(gray0, gray, thresholdLevel,
                Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                Imgproc.THRESH_BINARY,
                (src.width() + src.height()) / 200, t);
          }

          Imgproc.findContours(gray, contours, new Mat(),
              Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

          for (MatOfPoint contour : contours) {
            MatOfPoint2f temp = new MatOfPoint2f(contour.toArray());

            double area = Imgproc.contourArea(contour);
            approxCurve = new MatOfPoint2f();
            Imgproc.approxPolyDP(temp, approxCurve,
                Imgproc.arcLength(temp, true) * 0.02, true);

            if (approxCurve.total() == 4 && area >= maxArea) {
              double maxCosine = 0;

              List<Point> curves = approxCurve.toList();
              for (int j = 2; j < 5; j++) {

                double cosine = Math.abs(angle(curves.get(j % 4),
                    curves.get(j - 2), curves.get(j - 1)));
                maxCosine = Math.max(maxCosine, cosine);
              }

              if (maxCosine < 0.3) {
                maxArea = area;
                maxId = contours.indexOf(contour);
              }
            }
          }
        }
      }

      if (maxId >= 0) {
        Rect rect = Imgproc.boundingRect(contours.get(maxId));

        Imgproc.rectangle(src, rect.tl(), rect.br(), new Scalar(255, 0, 0, .8), 1);

        int mDetectedWidth = rect.width;
        int mDetectedHeight = rect.height;

        System.out.println("Rectangle width :" + mDetectedWidth + " Rectangle height :" + mDetectedHeight);

      }

      Imgcodecs.imwrite(imgOutputPath + "_blue.png", src);
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  private static boolean imageProcess3(String imgFileName) {
    try {
      File enhandcedFolder = new File(ENHANCED_IMG_FILE_PATH);
      if (!enhandcedFolder.exists()) {
        enhandcedFolder.mkdirs();
      }
      String imgInputPath = IMG_FILE_PATH + imgFileName;
      String imgOutputPath = ENHANCED_IMG_FILE_PATH + imgFileName.replace(".png", "");
      Mat source = Imgcodecs.imread(imgInputPath, Imgcodecs.CV_LOAD_IMAGE_COLOR);
      if (source.empty()) {
        System.out.println("Error: no image found!");
        return false;
      }

      Mat imgGray = new Mat();
      source.copyTo(imgGray);
      Imgproc.cvtColor(source, imgGray, Imgproc.COLOR_BGR2GRAY);
      Imgcodecs.imwrite(imgOutputPath + "_gray.png", imgGray);

      //apply threshold method
      Mat imgThreshold = new Mat();
      imgGray.copyTo(imgThreshold);
//      Imgproc.adaptiveThreshold(imgGray, imgThreshold, 255,
//          Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY_INV, 61, 90);
      Imgproc.threshold(imgGray, imgThreshold, 20, 255,  Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_OTSU);
      Imgcodecs.imwrite(imgOutputPath + "_threshold.png", imgThreshold);

      //apply distance method
      Mat imgDistance = new   Mat(source.size(), CvType.CV_32F);
      Imgproc.distanceTransform(imgThreshold, imgDistance, Imgproc.CV_DIST_L2, Imgproc.CV_DIST_MASK_PRECISE);
      Imgcodecs.imwrite(imgOutputPath + "_distance.png", imgDistance);

      Mat imgThreshold2 = new Mat();
      imgDistance.copyTo(imgThreshold2);
      Imgproc.threshold(imgGray, imgThreshold, 8, 255,  Imgproc.THRESH_BINARY_INV);
      Imgcodecs.imwrite(imgOutputPath + "_threshold2.png", imgThreshold);

      // threshold the distance transform
      Mat dibw32f = new Mat(source.size(), CvType.CV_32F);
      final double SWTHRESH = 10.0;    // stroke width threshold
      Imgproc.threshold(imgDistance, dibw32f, SWTHRESH/2.0, 255, Imgproc.THRESH_BINARY);
      Mat dibw8u = new Mat(source.size(), CvType.CV_8U);
      dibw32f.convertTo(dibw8u, CvType.CV_8U);

      Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
      // open to remove connections to stray elements
      Mat cont = new Mat(source.size(), CvType.CV_8U);
      Imgproc.morphologyEx(dibw8u, cont, Imgproc.MORPH_OPEN, kernel);
      // find contours and filter based on bounding-box height
      final double HTHRESH = source.rows() * 0.5; // bounding-box height threshold

       //find contours
      List<MatOfPoint> contours = new ArrayList<>();
      Imgproc.findContours(dibw8u, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
      System.out.println("contours size= "+contours.size());
      Scalar borderColor = new Scalar(255, 255, 255);

      List<Point> digits = new ArrayList<Point>();    // contours of the possible digits

      // find the convexhull of the digit contours
      MatOfInt digitsHullIdx = new MatOfInt();
      MatOfPoint hullPoints = new MatOfPoint();
      hullPoints.fromList(digits);
//      Imgproc.convexHull(hullPoints, digitsHullIdx);

      Imgproc.drawContours(source, contours, 1, borderColor, 1);

      // find appropriate bounding rectangles
      for (MatOfPoint contour : contours) {
        MatOfPoint2f areaPoints = new MatOfPoint2f(contour.toArray());
        RotatedRect boundingRect = Imgproc.minAreaRect(areaPoints);

        double rectangleArea = boundingRect.size.area();

        // test min ROI area in pixels
        if (rectangleArea > 100) {
          Point rotated_rect_points[] = new Point[4];
          boundingRect.points(rotated_rect_points);

          Rect rect = Imgproc.boundingRect(new MatOfPoint(rotated_rect_points));
          Scalar green = new Scalar(255,0,0);
          // test horizontal ROI orientation
          if (rect.width > rect.height) {
            Imgproc.rectangle(source, rect.tl(), rect.br(), green, 1);
          }
        }
      }
      Imgcodecs.imwrite(imgOutputPath + "_contours.png", source);
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  private static double angle(Point p1, Point p2, Point p0) {
    double dx1 = p1.x - p0.x;
    double dy1 = p1.y - p0.y;
    double dx2 = p2.x - p0.x;
    double dy2 = p2.y - p0.y;
    return (dx1 * dx2 + dy1 * dy2)
        / Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2)
        + 1e-10);
  }
}
