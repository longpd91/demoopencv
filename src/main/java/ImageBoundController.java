import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImageBoundController {

  // the FXML button
  @FXML
  private Button btnFileChooser;
  // the FXML image view
  @FXML
  private ImageView mIvSource, mIvGray, mIvThreshold, mIvTemp, mIvGreen, mIvHSV, mIvBlue;

  @FXML
  protected void openImage(ActionEvent event) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("View Pictures");
    fileChooser.setInitialDirectory(new File(System.getProperty("user.home"))
    );
    fileChooser.getExtensionFilters().addAll(
//        new FileChooser.ExtensionFilter("All Images", "*.*"),
//        new FileChooser.ExtensionFilter("JPEG", "*.jpeg"),
//        new FileChooser.ExtensionFilter("JPG", "*.jpg"),
//        new FileChooser.ExtensionFilter("GIF", "*.gif"),
//        new FileChooser.ExtensionFilter("BMP", "*.bmp"),
        new FileChooser.ExtensionFilter("PNG", "*.png")
    );
    File file = fileChooser.showOpenDialog(new Stage());
    System.out.println(file.getPath());

    // effectively grab and process a single frame
    Mat source = Imgcodecs.imread(file.getPath(), Imgcodecs.CV_LOAD_IMAGE_COLOR);

    // convert and show the frame
    Image imageToShow = Utils.mat2Image(source);
//    updateImageView(mIvSource, imageToShow);

    // Convert BGR to gray
    Mat imgGray = new Mat();
    source.copyTo(imgGray);
    Imgproc.cvtColor(source, imgGray, Imgproc.COLOR_BGR2GRAY);
//    updateImageView(mIvGray, Utils.mat2Image(imgGray));

    //threshold image
    Mat imgThreshold = new Mat();
    Imgproc.threshold(imgGray, imgThreshold, 245, 255, Imgproc.THRESH_BINARY_INV); //change pixel value to 0 if pixel >200; 0: white
//    Imgproc.adaptiveThreshold(imgGray, imgThreshold,255, Imgproc.ADAPTIVE_THRESH_MEAN_C,
//          Imgproc., 15,40);

    List<Mat> listRgb = new ArrayList<Mat>(3);

    Core.split(source, listRgb);
    updateImageView(mIvSource, Utils.mat2Image(listRgb.get(0)));//blue
    updateImageView(mIvGray, Utils.mat2Image(listRgb.get(1)));//green
    updateImageView(mIvThreshold, Utils.mat2Image(listRgb.get(2)));//red

    Mat imgTemp = new Mat();
    Mat imgGreen = new Mat();

    Core.add(listRgb.get(0), listRgb.get(2), imgTemp);
    updateImageView(mIvTemp, Utils.mat2Image(imgTemp));

    Core.subtract(listRgb.get(1), imgTemp, imgGreen);
    updateImageView(mIvGreen, Utils.mat2Image(imgGreen));

    Mat imgHSV = new Mat();
    Imgproc.cvtColor(source, imgHSV, Imgproc.COLOR_BGR2HSV, 3);
    updateImageView(mIvHSV, Utils.mat2Image(imgHSV));

    Mat imgBlue = new Mat();
    Scalar lowerBlue = new Scalar(0, 100, 100);
    Scalar upperBlue = new Scalar(120, 255, 255);
    Core.inRange(imgHSV, lowerBlue, upperBlue, imgBlue);

//    Imgcodecs.imwrite("C:\\Users\\LONGPD\\Desktop\\_morphology.png", imgBlue);

    Mat imgMorphology = new Mat();
    Mat kernelOpen = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(5, 5));
    Imgproc.morphologyEx(imgBlue, imgMorphology, Imgproc.MORPH_OPEN, kernelOpen);

    Mat imgMorphologyClose = new Mat();
    Mat kernelClose = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(25, 25));
    Imgproc.morphologyEx(imgMorphology, imgMorphologyClose, Imgproc.MORPH_CLOSE, kernelClose);

    Imgcodecs.imwrite("C:\\Users\\LONGPD\\Desktop\\_morphology.png", imgMorphologyClose);

    Scalar red = new Scalar(183, 1, 25);
    List<MatOfPoint> contours = new ArrayList<>();
    Imgproc.findContours(imgMorphologyClose, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
//    Imgproc.drawContours(source, contours, -1, red, 2);
//    Imgproc.fillPoly(imgMorphologyClose, contours, new Scalar(255, 0, 0));
    System.out.println("size=" + contours.size());
//    ArrayList<Point> hullPoints = new ArrayList<Point>();


    for (MatOfPoint contour : contours) {
      System.out.println("item =" + contour.toArray().length);
      MatOfPoint2f areaPoints = new MatOfPoint2f(contour.toArray());
      RotatedRect boundingRect = Imgproc.minAreaRect(areaPoints);
      double rectangleArea = boundingRect.size.area();

// find bounding convex hull, approxPolyDP get simplifiy convex hull
//      MatOfInt hull = new MatOfInt();
//      Imgproc.convexHull(contour , hull);
//      for(int j=0; j < hull.toList().size(); j++) {
//        hullPoints.add(contour.toList().get(hull.toList().get(j)));
//      }
      if (rectangleArea > 1000) {
        Point rotated_rect_points[] = new Point[4];
        boundingRect.points(rotated_rect_points);
        Rect rect = Imgproc.boundingRect(new MatOfPoint(rotated_rect_points));

        // test horizontal ROI orientation
        System.out.println(rect.width + " - " + rect.height);
        float ratio = rect.width / rect.height;
        if (ratio >= 2 && ratio <= 7) {
          Imgproc.rectangle(source, rect.tl(), rect.br(), red, 2);
        }
      }
    }
//    for (int j = 0; j < hullPoints.size(); ++j) {
//      Imgproc.line(source, hullPoints.get(j), hullPoints.get((j+1)%hullPoints.size()), new Scalar(0,0,255), 3);
//    }
    Imgcodecs.imwrite("C:\\Users\\LONGPD\\Desktop\\_contours.png", source);
    updateImageView(mIvBlue, Utils.mat2Image(imgMorphology));
  }

  private void updateImageView(ImageView view, Image image) {
    Utils.onFXThread(view.imageProperty(), image);
  }

}
