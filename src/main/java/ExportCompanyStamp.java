import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ExportCompanyStamp {
  // load the native OpenCV library
  static {
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
  }

  public boolean doExportStamp(String textInput, String inputFilePath, String exportFilePath) {
    try {
      File fileInput = new File(inputFilePath);
      if (!fileInput.exists()) return false;
      Mat source = Imgcodecs.imread(fileInput.getPath(), Imgcodecs.CV_LOAD_IMAGE_COLOR);
      Scalar fontColor = new Scalar(11, 20, 199);//red
      int fontFace = Core.FONT_HERSHEY_PLAIN, thickness = 5;
      double fontScale = 4.0;
      int[] baseline = new int[1];
      Size textSize;
      double xTextLine1 = 0, yTextLine1 = 0, xTextLine2 = 0, yTextLine2 = 0;
      String textLine2 = "";

      textInput = textInput.toUpperCase();
      List<String> texts = stringToList(textInput.trim());
      int totalWords = texts.size();

      if (totalWords < 4) {
        textSize = Imgproc.getTextSize(textInput, fontFace, fontScale, thickness, baseline);
        xTextLine1 = source.width() / 2 - textSize.width / 2;
        yTextLine1 = source.height() / 2;
        textInput = "";
        for (String myText : texts) {
          textInput += " " + myText.trim();
        }
      } else if (totalWords < 9) {
        textInput = "";
        for (int i = 0; i < 2; i++) {
          textInput += " " + texts.get(i).trim();
        }
        textSize = Imgproc.getTextSize(textInput, fontFace, fontScale, thickness, baseline);
        xTextLine1 = source.width() / 2 - textSize.width / 2;
        yTextLine1 = source.height() / 2.6;

        for (int i = 2; i < totalWords; i++) {
          textLine2 += " " + texts.get(i).trim();
        }

        textSize = Imgproc.getTextSize(textLine2, fontFace, fontScale, thickness, baseline);
        xTextLine2 = source.width() / 2 - textSize.width / 2;
        yTextLine2 = source.height() / 2.0;

        Imgproc.putText(source, textLine2, new Point(xTextLine2, yTextLine2),
            Core.FONT_HERSHEY_PLAIN, fontScale, fontColor, thickness);
      }

      Imgproc.putText(source, textInput, new Point(xTextLine1, yTextLine1),
          Core.FONT_HERSHEY_PLAIN, fontScale, fontColor, thickness);
      Imgcodecs.imwrite(exportFilePath, source);//sample exportFilePath : "C:\\Users\\LONGPD\\Desktop\\_contours.png"
      return true;
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return false;
    }
  }

  private static List<String> stringToList(String input) {
    if (input == null || input.equals("")) return new ArrayList<>();
    return Arrays.stream(input.split("\\s+")).collect(Collectors.toList());
  }

}
