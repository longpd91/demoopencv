import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.ghost4j.analyzer.AnalysisItem;
import org.ghost4j.analyzer.FontAnalyzer;
import org.ghost4j.converter.PDFConverter;
import org.ghost4j.document.PDFDocument;
import org.ghost4j.document.PSDocument;
import org.ghost4j.renderer.SimpleRenderer;
import org.opencv.core.Core;
import sun.misc.IOUtils;

public class ConvertPDF2Image {


  public static void main(String[] args) throws IOException {

    try {
      // load PDF document
      PDFDocument document = new PDFDocument();
      document.load(new File("D:\\LONGPD\\Git_Project\\DemoOpenCV/1507600338618.pdf"));
      // create renderer
      SimpleRenderer renderer = new SimpleRenderer();
      // set resolution (in DPI)
      renderer.setResolution(200);
      // copy file gsdll64.dll vao C:\Windows\System32
      // render
      List<Image> images = renderer.render(document);
      // write images to files to disk as PNG
      try {
        for (int i = 0; i < images.size(); i++) {
          ImageIO.write((RenderedImage) images.get(i), "png", new File((i + 1) + ".png"));
        }
      } catch (IOException e) {
        System.out.println("ERROR: " + e.getMessage());
      }

    } catch (Exception e) {
      System.out.println("ERROR: " + e.getMessage());
    }

  }
}
