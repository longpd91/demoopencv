import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.opencv.core.Core;

public class ImageBoundGUI extends Application {

  public static void main(String[] args) {
    // load the native OpenCV library
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    try {
      // load the FXML resource
      FXMLLoader loader = new FXMLLoader(getClass().getResource("ImageBoundGUI.fxml"));
      // store the root element so that the controllers can use it
      FlowPane rootElement = (FlowPane) loader.load();
      // create and style a scene
      Scene scene = new Scene(rootElement, 900, 600);
//      scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
      // create the stage with the given title and the previously created
      // scene
      primaryStage.setTitle("JavaFX meets OpenCV");
      primaryStage.setScene(scene);
      // show the GUI
      primaryStage.show();

      // set the proper behavior on closing the application
      ImageBoundController controller = loader.getController();
      primaryStage.setOnCloseRequest((new EventHandler<WindowEvent>() {
        public void handle(WindowEvent we) {
//          controller.setClosed();
        }
      }));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}